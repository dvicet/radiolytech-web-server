const { jwtVerify } = require('jose/jwt/verify');
const crypto = require('crypto');
const fs = require('fs');

module.exports = async (req, res, next) => {
  const token = req.headers.authorization;
  verify(token)
    .then(() => {
      next();
    })
    .catch(error => {
      res.status(401).json({ error: "Requête non authentifiée"});
    });
}

const verify = (jwt) => {
  return new Promise((resolve, reject) => {
    fs.readFile(
      './keys/' + process.env.JWT_PUBLIC_KEY_NAME,
      (err, data) =>
    {
      if (err) {
        reject(err)
      } else {
        const publicKey = crypto.createPublicKey(data);

        jwtVerify(jwt, publicKey, {
          issuer: 'radiolytech',
          audience: 0
        })
          .then((payload, protectedHeader) => {
            //console.log(protectedHeader);
            //console.log(payload);
            resolve();
          })
          .catch(error => {
            reject(error);
          })
      }
    });
  });
};
