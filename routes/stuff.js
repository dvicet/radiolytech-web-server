const express = require('express');
const router = express.Router();

const controller = require('../controller/stuff');
const adminAuth = require('../middleware/auth');

router.get('/song', controller.getCurrentSong);
router.get('/playlist', controller.getSongs);
router.get('/next', controller.getNextSong);
router.get('/playnext', adminAuth, controller.playNextSong);
router.post('/add', adminAuth, controller.addSong);
router.delete('/remove', adminAuth, controller.deleteSong);

module.exports = router;
