FROM node:alpine
ENV NODE_ENV=production

RUN apk add --no-cache ffmpeg

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install --production

COPY . .

VOLUME /app/songs

CMD ["node", "index.js"]
