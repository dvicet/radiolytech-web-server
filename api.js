const express = require('express');
const api = express.Router();

const stuffRoutes = require('./routes/stuff');
const userRoutes = require('./routes/user');

const daoUser = require('./utils/databaseConnexion');

api.use('/', stuffRoutes);
api.use('/user', userRoutes);

module.exports = api;
