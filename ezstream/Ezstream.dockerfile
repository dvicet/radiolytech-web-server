FROM alpine:latest

RUN apk add --no-cache ezstream nodejs

VOLUME /songs

EXPOSE 8000/tcp

WORKDIR /config

COPY . .

ENTRYPOINT ["./entrypoint.sh"]

CMD ["ezstream", "-c", "ezstream-config.xml"]
