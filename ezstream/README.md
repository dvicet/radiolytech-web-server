Exécuter Ezstream sans docker
----

1. À la racine du dépôt, avoir un dossier `songs` contenant toutes les musiques

2. Exécuter la commande suivante dans le dossier `ezstream` :

```sh
ezstream -c ezstream-config.xml
```
