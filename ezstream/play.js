#!/usr/bin/node

//const https = require('https')
const https = require('http');

const getToken = () => {
  return new Promise((resolve, reject) => {
    const userData = JSON.stringify({
      user: process.env.APP_ADMIN_LOGIN,
      password: process.env.APP_ADMIN_PASSWORD
    })

    const loginOptions = {
      hostname: process.env.APP_HOSTNAME || 'localhost',
      port: process.env.APP_PORT || 3141,
      path: '/user/login',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    }

    const reqLogin = https.request(loginOptions, res => {
      let data = '';

      res.on('data', (chunk) => {
        data += chunk;
      });

      res.on('end', () => {
        // print the filename of the next song
        try {
          let responseData = JSON.parse(data)
          let token = responseData.token;
          resolve(token);
        } catch (error) {
          reject(error)
        }
      })
    })

    reqLogin.on('error', error => {
      reject(error)
    })

    reqLogin.write(userData)
    reqLogin.end()
  })
};

getToken()
  .then(async (token) => {
    await getNextSong(token)
      .then(song => console.log(song))
      .catch(error => {
        throw error;
      })
  })
  .catch(() => {
    // Default song if error
    console.log("Moonlight_Sonata_Allegretto.ogg");
  })

const getNextSong = (token) => {
  return new Promise((resolve, reject) => {
    const options = {
      hostname: process.env.APP_HOSTNAME || 'localhost',
      port: process.env.APP_PORT || 3141,
      path: '/playnext',
      method: 'GET',
      headers: {
        'Authorization': token
      }
    }

    const req = https.request(options, res => {
      let data = '';

      res.on('data', (chunk) => {
        data += chunk;
      });

      res.on('end', () => {
        // print the filename of the next song
        try {
          let metadata = JSON.parse(data)
          resolve("../songs/" + metadata.artist + "/" + metadata.album + "/" + metadata.filename)
        } catch (error) {
          reject(error)
        }
      })
    })

    req.on('error', error => {
      reject(error)
    })

    req.end()
  });
}
