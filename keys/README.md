# Générer une clé privée et publique RSA

1. Générer la clé privée :

```sh
openssl genrsa -out key-jwt.pem
```

2. Obtenir la clé publique à partir de la clé générée :

```sh
openssl pkey -in key-jwt.pem -pubout -out key-jwt.pub
```
