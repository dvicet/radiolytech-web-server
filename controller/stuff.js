const playlist = require('../utils/mainplay');
const Song = require('../utils/Song');
const outputMessage = require('../utils/messages');

exports.getCurrentSong = (req,res) => {
	res.json(playlist.getCurrentSong());
};

exports.getSongs = (req, res) => {
	res.json(playlist.getSongs());
};

exports.getNextSong = (req,res) => {
	res.json(playlist.getNextSong());
};

exports.playNextSong = (req,res) => {
  let io = req.app.get('socketio');
  let nextsong = playlist.playNextSong();

	if(typeof nextsong !== 'undefined')
	{
		io.emit('changemusic', outputMessage(nextsong.getSong(), nextsong.getArtist()));
	}
	res.json(nextsong);
};

exports.addSong = (req, res) => {
	let name = req.body.name;
	let artist = req.body.artist;
	console.log(name)
	if(name !== null)
	{
      let song = new Song()
			song.takeMetadata(name);
      song.on('metadata', () => {
				song.makeDirectory();
				song.downloadSong();
        song.on("song", () => {
					playlist.addSong(song);
          res.status(200).json(playlist);

        });
			});

      song.on('error', (err) => {
        res.status(400).json({ error: err });
      });
	}
	else{
		res.status(400).json({ error: "missing name attribute" });
	}
};

exports.deleteSong = (req,res) => {
	let name = req.body.name;
	let r = playlist.songs.find(song => song.getFileName() === name);
	let index = playlist.songs.indexOf(r);
	if(index > -1)
	{
		playlist.removeSong(index);
	}
	res.json(playlist.getSongs());
};
