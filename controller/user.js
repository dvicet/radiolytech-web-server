const bcrypt = require('bcryptjs');
const { SignJWT } = require('jose/jwt/sign');
const crypto = require('crypto');
const fs = require('fs');
const daoUser = require('../utils/databaseConnexion');

exports.signup = (req, res) => {
  bcrypt.hash(req.body.password, 10)
    .then(hash => {
      // save in database
      daoUser.insertUser(req.body.user, hash)
      res.status(201).json({ hash: hash });
    })
    .catch(error => res.status(500).json({ error: error }));
};

exports.login = (req, res) => {
  daoUser.getUser(req.body.user)
    .then(async (results) => {
      let user = results[0];
      if(!user) {
        throw "Utilisateur non trouvé";
      }
      await bcrypt.compare(req.body.password, user.password)
        .then(valid => {
          if (!valid) {
            throw "mot de passe incorrect";
          }
          // Password is correct => give a JSON Web Token
          fs.readFile(
            './keys/' + process.env.JWT_PRIVATE_KEY_NAME,
            async (err, data) =>
            {
            if (err) throw err;

            let token = await sign(data, user.pseudo, user.role);
            res.status(200).json({
              token: token
            })
          });
        })
        .catch(error => {
          throw error;
        });
    })
    .catch(() => {
      res.status(401).json({ error: "Utilisateur / mot de passe incorrect"});
    });
};

const sign = async (data, user, role) => {
  const privateKey = crypto.createPrivateKey(data);

  const jwt = await new SignJWT({
    'user': user,
    'role': role
   })
    .setProtectedHeader({ alg: 'RS256' })
    .setIssuedAt()
    .setIssuer('radiolytech')
    .setAudience(role) // role in audience to verify that it is an admin token
    .setExpirationTime('2h')
    .sign(privateKey)
    .catch(error => { throw error });

  return jwt;
};
