window.onload = function() {
	var audio = document.getElementById("song");
	var volumeBar = document.getElementById("volume-bar");
	var playButton = document.getElementById("play-pause");
	var refreshButton = document.getElementById("refresh");

	var volume_icon = document.getElementById("volume_icon");
	var play_icon = document.getElementById("play_icon");

	audio.volume = 0.2;
	volumeBar.value = audio.volume;

	volumeBar.addEventListener("change", function() 
	{
		audio.volume = volumeBar.value;
		if(volumeBar.value == 0)
		{
			volume_icon.setAttribute("name", "volume-mute-outline");
		}
		else if(volumeBar.value < 0.3)
		{
			volume_icon.setAttribute("name", "volume-low-outline");
		}
		else if(volumeBar.value < 0.7)
		{
			volume_icon.setAttribute("name", "volume-medium-outline");
		}
		else
		{
			volume_icon.setAttribute("name", "volume-high-outline");
		}
	});

	playButton.addEventListener("click", function() 
	{
		if(audio.paused)
		{	
			audio.load();
			audio.play();
			play_icon.setAttribute("name", "pause-outline");
			document.getElementById("playtext").innerHTML = "Pause";
			
		}
		else
		{
			audio.pause();
			play_icon.setAttribute("name", "play-outline");
			document.getElementById("playtext").innerHTML = "Play";
		}
	});


	refreshButton.addEventListener("click", function()
	{
		location.reload();
	})

}




