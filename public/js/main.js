const socket = io();

const musicContainer = document.getElementById('music_container');
var title = document.getElementById("title");
var artist = document.getElementById("artist");


socket.on('welcome', message => {
	//createAudio();
	console.log(message);
	title.innerHTML = message.title;
	artist.innerHTML = message.artist;

});

socket.on('test', test => {
	console.log(test);
	title.innerHTML = test.title;
	artist.innerHTML = test.artist;
});

socket.on('changemusic', message => {
	console.log("Next song incoming")
	title.innerHTML = message.title;
	artist.innerHTML = message.artist;
});

function createAudio()
{
	var a = document.createElement("audio");
	a.setAttribute("id", "song");
	a.setAttribute("controls", "controls");
	a.setAttribute("autoplay", "autoplay");
	document.querySelector(".audio_div").appendChild(a);
}

function sleep(ms)
{
	return new Promise( resolve => setTimeout(resolve, ms));
}