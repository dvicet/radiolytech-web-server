function outputMessage(title, artist)
{
	return {title, artist};
}

module.exports = outputMessage;