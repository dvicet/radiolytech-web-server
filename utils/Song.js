const Spotify = require('../utils/Spotify');
const Youtube = require('../utils/Youtube');
const fs = require('fs');
const ffmetadata = require('ffmetadata');
const EventEmitter = require("events");


class Song extends EventEmitter{
  constructor() {
    super();
  }

  takeMetadata(name) {
    let spo = new Spotify(name);
    spo.on("Spotify", () => {
      let data = spo.getData();
      this.data = data;
      this.data.song = this.data.song.replace(/\//g, "-");
      this.data.artist = this.data.artist.replace(/\//g, "-");
      this.data.album = this.data.album.replace(/\//g, "-");
      console.log(data);
      this.filename = this.data.song + ".ogg";
      this.song = this.data.song;
      this.album = this.data.album;
      this.artist = this.data.artist;
      this.emit("metadata");
    });
  }

  makeDirectory() {
    var dir = __dirname + '/../songs/' + this.artist;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    if (!fs.existsSync(dir + '/' + this.album)) {
      fs.mkdirSync(dir + '/' + this.album);
    }
  }

  downloadSong() {
    let youtubeSong;
    if(this.artist!="unknown artist") {
      youtubeSong = new Youtube(this.artist + " " + this.song);
    }
    else {
      youtubeSong = new Youtube("" + (this.song));//encodeURI
    }
    youtubeSong.searchAndDownload(this.artist, this.album, this.song);
    youtubeSong.on("fileDownloaded", () => {
      this.writeMetadata();
      this.emit("song");
    });
  }

  writeMetadata() {
    ffmetadata.write("songs/" + this.artist + '/' + this.album + '/' + this.filename, this.data, function(err) {
      if (err) console.error("Error writing metadata", err);
    })
  }

  getFileName() {
    return this.filename;
  }

  setFileName(song_filename) {
   this.filename = song_filename;
  }

  getSong() {
    return this.song;
  }

  setSong(song_song) {
    this.song = song_song;
  }

  getArtist() {
    return this.artist;
  }

  setArtist(song_artist) {
    this.artist = song_artist;
  }

  getAlbum() {
    return this.album;
  }

  setAlbum(song_album) {
    this.album = song_album;
  }
}

module.exports = Song;
