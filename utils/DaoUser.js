var mysql = require("mysql2");
require('dotenv').config();

class DaoUser {
  constructor() {
  }

  createConnection() {
    this.connection = mysql.createConnection({
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME
    });
  }

  insertUser(user, password, role=1) {
    this.connection.execute(
      'INSERT INTO Users(pseudo, password, role) VALUES(?,?,?)',
      [user, password, role],
      function(err, results, fields) {
        console.log(results); // results contains rows returned by server
        console.log(fields); // fields contains extra meta data about results, if available
      }
    );
  }

  getUser(user) {
    return new Promise((resolve, reject) => {
      this.connection.execute(
        'SELECT pseudo, password, role FROM Users WHERE pseudo = ?',
        [user],
        (err, results) => {
          resolve(results);
        }
      );
    });
  }

  getTableElements(table) {
    this.connection.execute(
      'SELECT * FROM ?',
      [table],
      function(err, results, fields) {
        console.log(results); // results contains rows returned by server
      }
    );
  }
}

module.exports = DaoUser;
