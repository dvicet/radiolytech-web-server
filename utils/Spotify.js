const https = require('https')
const EventEmitter = require("events");

class Spotify extends EventEmitter{
	constructor(req) {
		super();
		this.data = {};
		console.log("spo");
		this.getToken(req);
	}

	metadata(name, token) {
		const options = {
			hostname: 'api.spotify.com',
			port: 443,
			path: '/v1/search?q='+encodeURI(name)+'&type=track&limit=1',
			method: 'GET',
			headers: {
				'Authorization': 'Bearer ' + token
			}
		}

		let req = https.request(options, (res) => {
			let todo = '';
			let data = {};

			res.on('data', (d) => {
				todo += d;
			});

			res.on('end', () => {
				try {
					let items = JSON.parse(todo).tracks.items;
					data = {
						'artist': items[0].artists[0].name,
						'album': items[0].album.name,
						'song': items[0].name
					};

					this.data = data;
					this.emit("Spotify");
				}
				catch(e) {
					console.log("yo");
					data = {
						'artist': 'unknown artist',
						'album': 'unknown album',
						'song': name
					};
					this.data = data;
					this.emit("Spotify");
				}
				return res;
			});
		});

		req.on('error', error => {
			console.error(error);
		});

		req.end();
	}

	async getToken(name) {
		const options = {
			hostname: 'accounts.spotify.com',
			port: 443,
			path: '/api/token?grant_type=client_credentials',
			method: 'POST',
			headers: {
				'Authorization': 'Basic ' + Buffer.from(process.env.CLIENT_ID + ':' + process.env.CLIENT_SECRET, 'utf-8').toString('base64'),
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}

		const req = https.request(options, res => {
			res.on('data', (d) => {
				let token = JSON.parse(d).access_token;
				this.metadata(name, token);
			})
		})

		req.on('error', error => {
			console.error(error);
		})

		req.end();

	}

	getData() {
		return this.data;
	}
}

module.exports = Spotify;
