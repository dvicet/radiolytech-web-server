const EventEmitter = require("events");

class Playlist {
  constructor(name) {
    this.songs = [];
    this.name = name;
  }

  addSong(song) {
    this.songs.push(song);
    return this.songs;
  }

  getCurrentSong() {
    return this.songs[0];
  }

  getSongs() {
    return this.songs;
  }

  removeSong(id) {
    if (id > -1 && id < this.songs.length) {
      this.songs.splice(id, 1);
    }
  }

  getNextSong() {
    if (this.songs.length > 1) {
      return this.songs[1];
    } else {
      return this.songs[0];
    }
  }

  playNextSong() {
    if (this.songs.length > 1) {
      this.songs.splice(0, 1);
    }
    return this.songs[0];
  }
}

module.exports = Playlist;
