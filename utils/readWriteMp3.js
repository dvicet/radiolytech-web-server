const path = require('path')
const fs = require('fs')
const ffmetadata = require("ffmetadata")

ffmetadata.read("../music/malik.mp3", function(err,data) {
	if(err) console.error("Error reading metadata", err);
	else console.log(data);
})

data = {
	artist: 'Népal',
	title: 'Malik Al Mawt',
	album_artist: 'Népal',
	TCM: 'klm (78e Session)',
	album: '444Nuits (Version Bleue',
	genre: 'Hip-Hop',
	date: '2016',
	encoder: 'Lavf58.29.100'
}

ffmetadata.write("music/malik2.mp3", data, function(err) {
	if (err) console.error("Error writing metadata", err);
	else console.log("Data written");
})