const DaoUser = require('./DaoUser');

// create the connection to database
const daoUser = new DaoUser();
daoUser.createConnection();

module.exports = daoUser;
