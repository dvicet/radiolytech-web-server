const fs = require('fs');
const ytsr = require('ytsr');
const ytdl = require('ytdl-core');
const ffmpeg = require('fluent-ffmpeg');
const EventEmitter = require("events");

class Youtube extends EventEmitter {
  constructor(query) {
    super();
    this.token = process.env.ZAK_YOUTUBE_TOKEN;
    this.query = query;
    this.artist = '';
    this.title = '';
    this.url = '';
  }

  async searchAndDownload(artist, album, song)
  {
    let filename = artist + '/' + album + '/' + song;
    const filterQuery = await ytsr.getFilters(this.query);
    const filterType = filterQuery.get('Type').get('Video');
    const options = {
      limit: 1,
    }
    try {
      const searchResults = await ytsr(filterType.url, options);
      //this.artist = searchResults.items[0].author.name;
      //this.title = searchResults.items[0].title;
      this.url = searchResults.items[0].url;
      console.log("Found on YouTube : " + this.title);
    } catch (e) {
      console.error(e);
      this.emit("error");
    }

    //Test if mp4 file is here
    fs.access('songs/' + filename + ".mp4", (err) => {
      // If we can access the file, it already exists
      if (!err) {
        console.error(filename + ' already exists');
      } else {

        //Test if mp3 file is here
        fs.access('songs/' + filename + ".ogg", (err) => {
          if (!err) {
            console.error(filename + ' already exists');
            this.emit("fileDownloaded");
          } else {
            this.download(this.url, filename);
          }

        });
      }
    });
  }

  async download(url, name)
  {
    let filename = 'songs/' + name;

    let writer = fs.createWriteStream(filename + '.mp4');

    let reader = ytdl(url,
      {quality: 'highestaudio',
       filter: 'audioonly'});

    // Create Youtube stream of the video
    reader.pipe(writer);

    // When the song is entirely downloaded
    reader.on('end', () => {
      console.log(filename + " downloaded");
      // Convert to MP3 with ffmpeg
      let command = ffmpeg(filename + '.mp4')
        .save(filename + '.ogg');

      // Delete MP4 file
      command.on('end', () => {
        fs.unlink(filename + '.mp4', (err) => {
          if (err) throw err;
          this.emit("fileDownloaded");
        });
      });
    });
  }

  getArtist() {
    return this.artist;
  }

  getTitle() {
    return this.title;
  }

}

module.exports = Youtube;
