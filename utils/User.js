class User {
  constructor(pseudo, password, role) {
    this.pseudo = pseudo;
    this.password = password;
    this.role = role;
  }

  setPseudo(pseudo) {
    this.pseudo = pseudo;
  }

  getPseudo() {
    return this.pseudo;
  }

  setPassword(password) {
    this.password = password;
  }

  getPassword() {
    return this.password;
  }

  setRole(role) {
    this.role = role;
  }

  getRole() {
    return this.role;
  }
}
