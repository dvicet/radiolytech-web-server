# Radiolytech Web Server

Node.js web server to stream the radio of Polytech Angers

# Variables d'environnement

Créer un fichier `.env` à la racine du dépôt :
```
# Node.js app
# Node.js app
APP_HOSTNAME=app
APP_PORT=3141
APP_ADMIN_LOGIN=admin
APP_ADMIN_PASSWORD=app_admin_password

# icecast
ICECAST_HOSTNAME=icecast
ICECAST_SOURCE_PASSWORD=source-password
ICECAST_ADMIN_PASSWORD=admin-password

# Spotify
CLIENT_ID=
CLIENT_SECRET=

# Discord
DISCORD_TOKEN=
BOT_ACCOUNT_PASSWORD=bot_password

# Database
DB_HOST=db
DB_ROOT_PASSWORD=root_dev_password
DB_PASSWORD=dev_password
DB_NAME=radiolytech
DB_USER=radiolytech

# RSA Private and public key names for JWT
JWT_PRIVATE_KEY_NAME=key-jwt-dev.pem
JWT_PUBLIC_KEY_NAME=key-jwt-dev.pub

```

# Développement avec Docker

à la racine du dépôt, lancer la commande :

* Pour **construire** les conteneurs :
```
docker-compose -f docker-compose.dev.yml build
```

* Pour les **lancer** :
```
docker-compose -f docker-compose.dev.yml up
```

* Pour **stopper et supprimer** les conteneurs :
```
docker-compose -f docker-compose.dev.yml down
```

# Déploiement en production

Pareil que pour le développement, mais on utilise le fichier `docker-compose.yml` :
```
docker-compose up
```
