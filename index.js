const express = require('express')
const http = require('http')
const path = require('path')
const socketio = require('socket.io')
const bodyParser = require('body-parser')

const playlist = require('./utils/mainplay')
const api = require('./api')

require('dotenv').config();

const app = express()
const port = process.env.APP_PORT || 3141;
const server = http.createServer(app);

const io = socketio(server);

app.set('socketio', io);

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, "public")));


const outputMessage = require('./utils/messages');
io.on('connection', socket => {
	console.log("connexion");
	let currentsong = playlist.getCurrentSong()
	if(typeof currentsong !== 'undefined')
	{
		socket.emit("welcome", outputMessage(currentsong.getSong(),currentsong.getArtist()));
	}
})


/** ROUTES DE L'API**/
// app.use('/', require('./routes/api'));
app.use('/', api);

server.listen(port, () => {
  console.log('Server listening on port ' + port);
});

module.exports = app;
