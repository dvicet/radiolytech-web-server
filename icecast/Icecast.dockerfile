FROM debian:latest

RUN apt-get update \
    && apt-get install -y icecast2

WORKDIR /config

COPY . .

EXPOSE 8000/tcp

ENTRYPOINT ["./entrypoint.sh"]

CMD ["icecast2", "-c", "icecast-config.xml"]
