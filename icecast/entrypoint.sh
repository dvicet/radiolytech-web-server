#!/bin/sh

if [ -z $ICECAST_HOSTNAME ]
then
    ICECAST_HOSTNAME='localhost'
fi
if [ -z $ICECAST_SOURCE_PASSWORD ]
then
    ICECAST_SOURCE_PASSWORD='hackme'
fi
if [ -z $ICECAST_ADMIN_PASSWORD ]
then
    ICECAST_ADMIN_PASSWORD='hackme'
fi

set_value() {
    echo $1 : $2
    sed -i "s/<$1>.*<\/$1>/<$1>$2<\/$1>/g" icecast-config.xml
}

set_value hostname $ICECAST_HOSTNAME
set_value source-password $ICECAST_SOURCE_PASSWORD
set_value admin-password $ICECAST_ADMIN_PASSWORD

exec "$@"
