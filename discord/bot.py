#!/bin/python

import discord
import os

from dotenv import load_dotenv
from discord.ext import commands
from discord.utils import get
import requests
import json

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
APP_HOSTNAME = os.getenv('APP_HOSTNAME', 'localhost')
APP_PORT = os.getenv('APP_PORT', '3141')
ICECAST_HOSTNAME = os.getenv('ICECAST_HOSTNAME', 'localhost')

APP_ADMIN_LOGIN = os.getenv('APP_ADMIN_LOGIN')
APP_ADMIN_PASSWORD = os.getenv('APP_ADMIN_PASSWORD')


def getToken():
    url="http://" + APP_HOSTNAME + ":" + APP_PORT + "/user/login"
    token = requests.post(
        url,
        json=
        {
            'user': APP_ADMIN_LOGIN,
            'password': APP_ADMIN_PASSWORD
        },
        headers={'Content-Type':'application/JSON'}
    )
    return json.loads(token.text)['token']

bot = commands.Bot(command_prefix='$')

@bot.command(name='complètement')
async def say(ctx, texte):
    response = "I agree with you."
    await ctx.send(response)

@bot.command(name='yn')
async def questionne(ctx, *texte):
    response = await ctx.send(" ".join(texte))
    await response.add_reaction("✅")
    await response.add_reaction("❌")
    await response.add_reaction("❔")
    await ctx.message.delete()

@bot.command(pass_context=True, aliases=['j','joi'])
async def join(ctx):
    global voice
    channel = ctx.message.author.voice.channel
    voice = get(bot.voice_clients, guild=ctx.guild)

    if voice and voice.connected():
        await voice.move_to(channel)
    else:
        voice = await channel.connect()
    await voice.disconnect()

    if voice and voice.is_connected():
    	await voice.move_to(channel)
    else:
    	voice = await channel.connect()

    voice.play(discord.FFmpegPCMAudio(
        "http://" + ICECAST_HOSTNAME + ":8000/stream"
    ))

    await ctx.send(f"Joined {channel}\n")

@bot.command(pass_context=True, aliases=['l','lea'])
async def leave(ctx):
	channel = ctx.message.author.voice.channel
	voice = get(bot.voice_clients, guild=ctx.guild)
	if voice and voice.is_connected():
		await voice.disconnect()
		await ctx.send(f"Left {channel}\n")
	else:
		await ctx.send(f"Not in a channel\n")

@bot.command(pass_context=True, aliases=['c', 'cur'])
async def current(ctx):
	url="http://" + APP_HOSTNAME + ":" + APP_PORT + "/song"
	currentSong=json.loads(requests.get(url).text)
	await ctx.send(currentSong['filename'][:-4])

@bot.command(pass_context=True, aliases=['pl', 'playl'])
async def playlist(ctx):
	url="http://" + APP_HOSTNAME + ":" + APP_PORT + "/playlist"
	playlist=json.loads(requests.get(url).text)
	plist=''
	for i in range(0,len(playlist)):
		plist+=playlist[i]["filename"][:-4]
		plist+='\n'
	await ctx.send(plist)

@bot.command(pass_context=True, aliases=['n', 'nex'])
async def next(ctx):
	url="http://" + APP_HOSTNAME + ":" + APP_PORT + "/next"
	nextSong=json.loads(requests.get(url).text)
	await ctx.send(nextSong['filename'][:-4])

@bot.command(pass_context=True, aliases=['pn', 'playn'])
async def playnext(ctx):
	url="http://" + APP_HOSTNAME + ":" + APP_PORT + "/playnext"
	nextSong=json.loads(requests.get(
        url,
        headers={
            'Authorization': getToken()
        }
        ).text)
	await ctx.send(nextSong['filename'][:-4])

@bot.command(pass_context=True, aliases=['p', 'pla', 'add'])
async def play(ctx, *, name):
	url="http://" + APP_HOSTNAME + ":" + APP_PORT + "/add"
	print(requests.post(
        url,
        json={'name': name},
        headers={
            'Content-Type':'application/JSON',
            'Authorization': getToken()
        }
        ).text)

@bot.command(pass_context=True, aliases=['r', 'rem'])
async def remove(ctx, name:str):
	url="http://" + APP_HOSTNAME + ":" + APP_PORT + "/remove"
	name=name+".mp3"
	print(requests.delete(
        url,
        json={'name': name},
        headers={
            'Content-Type':'application/JSON',
            'Authorization': getToken()
        }
        ).text)


bot.run(TOKEN)
