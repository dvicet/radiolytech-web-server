FROM python

RUN apt-get update \
    && apt-get install -y ffmpeg nodejs npm \
    && npm install -g nodemon

VOLUME /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

CMD [ "nodemon", "--exec", "python", "-u", "bot.py" ]
