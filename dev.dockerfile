FROM node:alpine

ENV NODE_ENV=development

RUN apk add --no-cache ffmpeg

WORKDIR /app

VOLUME /app

EXPOSE 3141/tcp

CMD ["npm", "run", "dev"]
